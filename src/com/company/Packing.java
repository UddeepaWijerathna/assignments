package com.company;

public class Packing implements Department {

    int productCount=0;

    public Packing(int pCount){
        productCount = pCount;
    }

    @Override
    public void increaseProductCount() {
        productCount += 5;
        System.out.println(productCount);
    }

    @Override
    public void decreaseProductCount() {
        if(productCount>=5){
            productCount -= 5;
            System.out.println(productCount);
        }
        else{
            System.out.println("0");
        }

    }
}
