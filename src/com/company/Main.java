package com.company;

public class Main {

    public static void main(String[] args) {
        Cutting cutting =new Cutting(10);
        cutting.increaseProductCount();
        cutting.decreaseProductCount();

        Sewing sewing = new Sewing(10);
        sewing.increaseProductCount();
        sewing.decreaseProductCount();

        Packing packing = new Packing(0);
        //packing.increaseProductCount();
        packing.decreaseProductCount();
    }
}
