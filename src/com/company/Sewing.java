package com.company;

public class Sewing implements Department {

    int productCount=0;

    public Sewing(int pCount){
        productCount = pCount;
    }

    @Override
    public void increaseProductCount() {
        productCount += 1;
        System.out.println(productCount);
    }

    @Override
    public void decreaseProductCount() {
        if(productCount>=1){
            productCount -= 1;
            System.out.println(productCount);
        }
        else{
            System.out.println("0");
        }

    }
}
