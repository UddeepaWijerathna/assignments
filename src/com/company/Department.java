package com.company;

public interface Department {

    public void increaseProductCount();
    public void decreaseProductCount();

}
